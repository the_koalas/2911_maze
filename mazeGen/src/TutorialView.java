import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class TutorialView extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private String background;
	private TutorialScreen ts;
	
	TutorialView(int width, int height, TutorialScreen ts) {
		this.ts = ts;
		this.background = "res/backdrop.png";	
		this.setPreferredSize(new Dimension(width, height));
		this.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		addFeatures();
	}
	
	private void addFeatures() {
		JPanel tp = new JPanel();
		tp.setLayout(new BoxLayout(tp, BoxLayout.Y_AXIS));
		tp.setBorder(new EmptyBorder(new Insets(2,2,2,2)));
		tp.setMinimumSize(new Dimension(320, 400));
		tp.setPreferredSize(new Dimension(320, 400));
		tp.setMaximumSize(new Dimension(320, 400));
		
		
		JLabel controls = createDescription("Use the arrow keys to move your character!", "res/playerDown.png");;
		JLabel winCondition = createDescription("<html><b>Get to the goal to win!</b></html>", "res/goal.png");
		JLabel mysteryTile = createDescription("<html><br><b>Mystery Tiles!</html>", "res/mystery.png");
		JLabel solution = createDescription("<html><br><b>Hold V to show a solution!</b></html>", "res/leaf.png");
		JLabel bomb = createDescription("<html><br><b>Press B to use a Bomb!</b></html>", "res/bomb.png");
		
		JLabel health = new JLabel("<html>If your health runs out, you lose!</html>", JLabel.CENTER);
		JLabel mysteryExplanation = new JLabel("<html>&nbsp&nbsp&nbsp&nbsp Step on this tile for an equal<br>chance to get a bonus or a penalty!", JLabel.CENTER);
		JLabel condition = new JLabel("<html>Watch out! Using this will drain your Health!</html>", JLabel.CENTER);
		JLabel bombInfo = new JLabel("<html>Destroys a wall in the direction you face!</html>", JLabel.CENTER);
		
		tp.add(controls);
		tp.add(winCondition);
		tp.add(health);
		tp.add(mysteryTile);
		tp.add(mysteryExplanation);
		tp.add(solution);
		tp.add(condition);
		tp.add(bomb);
		tp.add(bombInfo);
		
		JButton close = createButton("CLOSE");
		close.addActionListener(this);
		close.setActionCommand("close");
		close.setForeground(new Color(200, 25, 25));
	
	
	
		
		this.add(tp);
		this.add(close);
	}
	
	private JLabel createDescription(String text, String image) {
		Image img = loadImage(image).getScaledInstance(25, 25, java.awt.Image.SCALE_SMOOTH);
		ImageIcon imgIcon = new ImageIcon(img);
		JLabel tutorialLabel = new JLabel(text, imgIcon, JLabel.CENTER);
		Font font = new Font("Calibri", Font.BOLD,11);
		tutorialLabel.setFont(font);
		tutorialLabel.setVerticalTextPosition(JLabel.TOP);
		tutorialLabel.setHorizontalTextPosition(JLabel.CENTER);
		return tutorialLabel;
	}
	
	private JButton createButton(String name) {
		this.add(Box.createRigidArea(new Dimension(0, 40)));
		JButton btn = new JButton(name);
		btn.setAlignmentX(Component.CENTER_ALIGNMENT);
		btn.setMinimumSize(new Dimension(200, 50));
		btn.setPreferredSize(new Dimension(200, 50));
		btn.setMaximumSize(new Dimension(200, 50));
		btn.setFont(new Font("Calibri", Font.BOLD, 16));
		return btn;
	}
	
	public Image loadImage(String src) {
		ImageIcon icon = new ImageIcon(src);
		Image img = icon.getImage();
		return img;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image background = loadImage(this.background);
		g.drawImage(background, 0, 0, null);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
		case "close":
			ts.getContentPane().removeAll();
			ts.dispose();
		}
	}
	
}
