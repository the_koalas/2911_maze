import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class SideMenu extends JPanel implements ActionListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int height;
	private int width;
	
	private Timer timer;
	private Bomb bomb;
	private Health health;
	private GridBagConstraints gbc;
	public static Color BACKGROUNDCOLOR;
	public static int fontcolor = 634010;
	private JButton menu;
	private JButton restart;
	private JButton quit;
	private JLabel box;
	private JLabel curLevel;
	private GameEngine ge;
	
	public SideMenu(int width, int height, GameEngine ge){ //ge not used
		this.height = height;
		this.width = width;
		this.ge = ge;
		
		BACKGROUNDCOLOR = new Color(238,252,206);
		
		setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		
		gbc.anchor = GridBagConstraints.NORTH;
		 //applies spacing
		
		box = new JLabel("<html><div style='background-color: #"+BACKGROUNDCOLOR.hashCode()+";'></div></html>");
		box.setPreferredSize(new Dimension(200, 200));
		
		curLevel = new JLabel("<html><p style='color:#"+fontcolor+"; text-align:center; font-size:15px; font-weight:bold; margin-bottom:15px'>" + "Level " + ge.getCurrentLevel() + "</p></html>");

		timer = new Timer();
		bomb = new Bomb();
		
		health = new Health();
		health.setPreferredSize(new Dimension(140, 68));
		
		
		menu = new JButton("<html><p style='color:#84bd00; padding: 5px 0px; width: 50px; font-weight:bold; font-size:12px; text-align:center;'>Menu</p></html>");
		quit = new JButton("<html><p style='color:#AA4D39; padding:5px 0px; width:50px; font-weight:bold; text-align:center; font-size:12px;'>Quit</p></html>");
		restart = new JButton("<html><p style='color:#84bd00; padding:5px 0; width:50px; text-align:center; font-weight:bold; font-size:12px;'>Reset</p></html>");
		
		menu.setBackground(Color.white);
		menu.addActionListener(this);
		menu.setActionCommand("menu");
		
		quit.addActionListener(this);
		quit.setActionCommand("quit");
		
		restart.addActionListener(this);
		restart.setActionCommand("restart");
		
		
		int base = 3;
		gbc.insets = new Insets(30,5,5,5);
		gbc.weightx = 0 ;
		gbc.weighty = 0.1;
		gbc.gridx = 1;
		gbc.gridy = base-2;
		this.add(box, gbc);

		gbc.insets = new Insets(5,5,5,5);
		gbc.weighty = 0;
		gbc.weightx = 0;
		gbc.gridx = 1;
		gbc.gridy = base - 1;
		this.add(curLevel, gbc);

		gbc.insets = new Insets(5,5,5,5);
		gbc.weighty = 0;
		gbc.gridx = 1;
		gbc.gridy = base;
		this.add(health, gbc);

		gbc.weightx = 0 ;
		gbc.weighty = 0.1;
		gbc.gridx = 1;
		gbc.gridy = base + 1;
		this.add(bomb, gbc);

		gbc.weightx = 0 ;
		gbc.weighty = 0;
		gbc.gridx = 1;
		gbc.gridy = base + 2;
		this.add(timer, gbc);

		gbc.weightx = 0 ;
		gbc.weighty = 0;
		gbc.gridx = 1;
		gbc.gridy = base + 3;
		this.add(menu, gbc);
		
		gbc.weightx = 0 ;
		gbc.weighty = 0;
		gbc.gridx = 1;
		gbc.gridy = base + 4;
		this.add(restart, gbc);
		
		gbc.weightx = 0 ;
		gbc.weighty = 0.5;
		gbc.gridx = 1;
		gbc.gridy = base + 5;
		this.add(quit, gbc);
		
		this.validate();
		
	}
	
	public void render(){
		
	}
	
	public void paintComponent(Graphics g){
		
		
		
		super.paintComponent(g);
		
		
		this.setOpaque(true);
		
		this.menu.repaint();
		this.quit.repaint();
		this.restart.repaint();
		this.box.repaint();
		this.curLevel.repaint();
		this.validate();
		
		
		Image bg2 = loadImage("res/menu.png"); //Get the image referenced by the tile
		g.drawImage(bg2, 0, 0, width, height, null);
		
	}
	
	public void actionPerformed (ActionEvent e){
		
		
		
		switch(e.getActionCommand()){
		case "quit":
			System.exit(1);
			break;
			
		case "menu":
			//Closes current game, brings up menu.
			ge.setGameState(GameState.Menu);
			
			break;
			
		case "restart":
			ge.setGameState(GameState.Restart);
			GameEngine.gameState = GameState.Restart;
			
			break;
		}
		
	}
	
	
	public Image loadImage(String src) {
		ImageIcon icon = new ImageIcon(src);
		Image img = icon.getImage();
		return img;
	}
	
}


