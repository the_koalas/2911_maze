
public interface DifficultyStrategy {
	public void generateMaze(Grid g, Node<Tile> goal);
}
