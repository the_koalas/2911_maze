import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Stack;

public class EasyDifficulty implements DifficultyStrategy{
	@Override
	public void generateMaze(Grid g, Node<Tile> goal) {

		Stack<Node<Tile>> st = new Stack<Node<Tile>>();
		HashMap<Node<Tile>, Boolean> visited = new HashMap<Node<Tile>, Boolean>();

		Node<Tile> randNeighbour;
		Node<Tile> curr = g.getNode(new CoOrd(0, 0)); //curr initially is start node
		ArrayList<Node<Tile>> neighbours = new ArrayList<Node<Tile>>();

		int rand = 0;
		int skewedRand = 0;
		int oldRand, commonWall;
		boolean success = false;
		boolean foundGoal = false;
		boolean complete = false;
		randInt(0, neighbours.size());
		//we've seen the start node
		visited.put(curr, true);
		//while we haven't seen the entire grid
		while (!complete) {
			if (visited.size() == (g.getGridSize()*g.getGridSize())) complete = true;
			neighbours = g.getNeighbours(curr); 
			
			oldRand = rand;
			
			skewedRand = randInt(0, 100);
			
			
			if (skewedRand < 70) {				
				rand = oldRand;
			} else {
				// get a random number betweeen 0 and the number of neighbours curr has
				rand = randInt(0, neighbours.size());
			}

			// set a found goal flag when goal found
			if(curr.equals(goal))  {
				foundGoal = true;
			}
			
			// for each of curr tile's neigbours
			for(int i=0; i<neighbours.size(); i++) {
				randNeighbour = neighbours.get((rand+i)%neighbours.size());
				//if naybr node doesn't exist or is not set as true
				//break walls, set as seen, update curr
				if (!visited.containsKey(randNeighbour) || !visited.get(randNeighbour)) {

					//push onto stack and mark as visited
					st.push(curr);
					visited.put(randNeighbour, true);
					commonWall = g.commonWall(curr, randNeighbour);
					g.breakWall(curr, randNeighbour, commonWall);


					if (!foundGoal) {
						curr.setBestMove(randNeighbour);
					} else if (randNeighbour.getBestMove() == null) {
						randNeighbour.setBestMove(curr);
					}
					randNeighbour.setPrev(curr);
					curr = randNeighbour;
					//repeat above process if
					//unseen neighbour
					success = true;
					break;
				}

			}

			/**
			 * if reached here no unseen
			 * neighbour was found, so
			 * we are to back track to the
			 * first node in the stack which
			 * has unseen neighbour, and repeat above.
			 */
			if (!success) {
				success = false;
				//case1: curr is a dead end
				//best move is to get to the
				//first node which has unvisited neighbours
				if (!st.isEmpty()) {

					//backtrack step
					if (!foundGoal) {
						curr.setBestMove(st.peek());
					}
					curr = st.pop();
				}
			} else {
				success = false;
			}
		}
		g.setLastVisited(curr);
		goal.setBestMove(null);
	}

	/**
	 * source: http://stackoverflow.com/questions/20389890
	 * /generating-a-random-number-between-1-and-10-java
	 * Returns a psuedo-random number between min and max, inclusive.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimim value
	 * @param max Maximim value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 */
	private int randInt(int min, int max) {
	
	    // Usually this can be a field rather than a method variable
	    Random rand = new Random();
	
	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	
	    return randomNum;
	}
	
}
