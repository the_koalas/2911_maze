import java.awt.BasicStroke;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JComponent;

public class GameView extends JComponent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Tile[][] tileMap; //The 2D array of tiles we will paint
	private int tileSize; //The size of the tiles in the array
	private boolean mapReady = false; //A flag to start painting the tiles
	private int width; //Global width
	private int height; //Global height
	private Player player;
	private String playerSrc = "res/player.gif"; //Image source for the player
	private int playerSize;
	private int playerX;
	private int playerY;
	private boolean showPath = false;
	private ArrayList<Tile> path = new ArrayList<Tile>();
	
	public GameView(int width, int height, Player player){
		this.width = width;
		this.height = height;
		this.player = player;
		this.setPreferredSize(new Dimension(width, height));
	}
	
	public void showPath(boolean show){
		showPath = show;
	}
	
	public void loadTileMap(Tile[][] tiles){ //Used to load the 2D array of tiles
		this.tileMap = tiles; //Load the tiles
		this.tileSize = this.tileMap[0][0].getTileSize(); //Set the tile size
		this.playerSize = (int) (tileSize*0.7);
		this.mapReady = true; //Flag to tell paint to start drawing the tiles
	}
	
	public void movePlayerToTile(int x, int y){ //Move the player to tile x, y
		int centerOffSet = (this.tileSize - this.player.getSize()) / 2;
		playerX = (tileSize) * x + centerOffSet; //Just mathhh
		playerY = (tileSize) * y + centerOffSet;
		this.player.getPosition().setX(x);
		this.player.getPosition().setY(y);
		repaint(); //Re-draw the player
	}
	
	public void drawPath(Graphics g){
		double radius = this.tileSize * 0.40;
		for(Tile t : this.path){
			int x = (int) ((t.getPosition().getX()* tileSize + t.getTileSize() / 2) - radius / 2);
			int y = (int) ((t.getPosition().getY()* tileSize + t.getTileSize() / 2) - radius / 2);
			Image leaves = loadImage("res/leaf.png");
			g.drawImage(leaves, x, y, (int)radius, (int)radius, null);
		}
	}
	
	public void clearPath(){
		this.path = new ArrayList<Tile>();
	}
	
	public void addToPath(Tile t){
		this.path.add(t);
	}
	
	public void drawWalls(Graphics g){
		for(int i = 0; i < tileMap[1].length ; i++){ //Loop through the tiles
			for(int j = 0; j < tileMap[0].length; j++){
				Tile tempTile = tileMap[i][j];
				
				Integer[] walls = tempTile.getWall();
				int strokeSize = 2;
				((Graphics2D) g).setStroke(new BasicStroke(strokeSize));
				
				Image hWallTop = loadImage("res/hWallUp.png");
				Image hWallDown = loadImage("res/hWallDown.png");
				
				Image vWallRight = loadImage("res/vWallRight.png");
				Image vWallLeft = loadImage("res/vWallLeft.png");
				
				int wallThick = (tileSize * 2 / 10);
				if(walls[0] == 1 || j == 0){ //Up
					g.drawImage(hWallTop, i * tileSize, j * tileSize , tileSize, wallThick, null);
				}
				if(walls[1] == 1 || i >= tileMap[1].length){ //Right
					g.drawImage(vWallRight, i * tileSize + tileSize - wallThick , j * tileSize, wallThick, tileSize, null);
				} 
				if(walls[2] == 1 || j >= tileMap[0].length - 1){ //Down
					
					g.drawImage(hWallDown, i * tileSize, j * tileSize + tileSize - wallThick, tileSize, wallThick, null);
				}
				if(walls[3] == 1 || i == 0){ //Left
					g.drawImage(vWallLeft, i * tileSize , j * tileSize, wallThick, tileSize, null);
				}

			}
		}
	}
	
	public void drawPlayer(Graphics g){
		switch(this.player.getDirection()){
		case UP:
			this.player.setImgSrc("res/playerUp.png");
			break;
			
		case RIGHT:
			this.player.setImgSrc("res/playerRight.png");
			break;
		
		case DOWN:
			this.player.setImgSrc("res/playerDown.png");
			break;
			
		case LEFT:
			this.player.setImgSrc("res/playerLeft.png");
			break;
			
		case NO:
			this.player.setImgSrc("res/playerDown.png");
		}
		
		Image playerImg = loadImage(this.player.getImgSrc()); //Get the player image
		int centerOffSet = (this.tileSize - this.player.getSize()) / 2;
		g.drawImage(playerImg, this.player.getPosition().getX() * tileSize + centerOffSet, this.player.getPosition().getY() * tileSize + centerOffSet, this.player.getSize(), this.getPlayer().getSize(), null); //Draw the player image
	}
	
	public void drawMap(Graphics g){
		for(int i = 0; i < tileMap[1].length ; i++){ //Loop through the tiles
			for(int j = 0; j < tileMap[0].length; j++){
				Tile tempTile = tileMap[i][j];
				Image img = loadImage(tempTile.getImgSrc()); //Get the image referenced by the tile
				
				g.drawImage(img, i * tileSize, j * tileSize, tileSize, tileSize, null); //Draw the image	
				
				
			}
		}
		
		//hacky draw mystery tile
		for (CoOrd coord: GameEngine.mysteryTiles){
			int i = coord.getX();
			int j = coord.getY();
			
			Image img = loadImage("res/mystery.png"); //Get the image referenced by the tile
			
			g.drawImage(img, i * tileSize, j * tileSize, tileSize, tileSize, null); //Draw the image	
			
		}
	}
	
	public int translate(int value){
		return 0;
	}
	
	public void paintComponent(Graphics g){
		if(mapReady){ //Checks if the map is ready to paint
			drawMap(g);
			drawWalls(g);
			if(showPath) drawPath(g);
			drawPlayer(g);
		}
	}
	
	public Image loadImage(String src){ //Just makes loading images easier
		ImageIcon icon = new ImageIcon(src);
		Image img = icon.getImage();
		
		return img;
	}

	// ============== GETTERS AND SETTERS =====================
	
	public Player getPlayer(){
		return this.player;
	}
	
	public void setPlayer(Player player){
		this.player = player;
	}
	
	/**
	 * @return the playerX
	 */
	public int getPlayerX() {
		return playerX;
	}

	/**
	 * @param playerX the playerX to set
	 */
	public void setPlayerX(int playerX) {
		this.playerX = playerX;
	}

	/**
	 * @return the playerY
	 */
	public int getPlayerY() {
		return playerY;
	}

	/**
	 * @param playerY the playerY to set
	 */
	public void setPlayerY(int playerY) {
		this.playerY = playerY;
	}

	public Tile[][] getTileMap() {
		return tileMap;
	}
	
	public void setTileMap(Tile[][] tileMap) {
		this.tileMap = tileMap;
	}
	
	public int getTileSize() {
		return tileSize;
	}
	
	public void setTileSize(int tileSize) {
		this.tileSize = tileSize;
	}
	
	public boolean isMapReady() {
		return mapReady;
	}
	
	public void setMapReady(boolean mapReady) {
		this.mapReady = mapReady;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}
	
	public String getPlayerSrc() {
		return playerSrc;
	}
	
	public void setPlayerSrc(String playerSrc) {
		this.playerSrc = playerSrc;
	}
	
	public int getPlayerSize() {
		return playerSize;
	}
	
	public void setPlayerSize(int playerSize) {
		this.playerSize = playerSize;
	}	
	
}
