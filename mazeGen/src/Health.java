import java.awt.BorderLayout;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Health extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel health;
	private float life = 100;
	private int width = 140;
	private int height = 68;
	
	/**
	 * Constructor for Health class, extends JPanel. Displays health bar on side menu.
	 */
	public Health(){
		
		//right now life depends on time
		life = 100;
		health = new JLabel("<html><p style='color:#"+SideMenu.fontcolor+"; text-align:center; font-size:15px; font-weight:bold; margin-bottom:10px;'>Health</p><div style='health:16px; width:100px; background-color:#ffffff;'><div style='height:16px; width:"+life+"px; background-color:#84bd00;'></div></div></html>");
		
	}
	
	public void paintComponent(Graphics g){
		g.setColor(SideMenu.BACKGROUNDCOLOR);
		
		g.drawRect(0, 0, width, height);
		g.fillRect(0, 0, width, height);
		
		
		//Create another global variable for indication of level.
		//Eg. level 1, life is dependant on time
		// level 2, life is dependant on monsters.
		life = (float)(GameEngine.TIMELIMIT-GameEngine.ELAPSEDTIME)/GameEngine.TIMELIMIT * 100;
		
		
		
		health = new JLabel("<html><p style='color:#"+SideMenu.fontcolor+"; text-align:center; font-size:15px; font-weight:bold; margin-bottom:10px;'>Health</p><div style='health:16px; width:100px; background-color:#ffffff;'><div style='height:16px; width:"+life+"px; background-color:#84bd00;'></div></div></html>");
		
		this.removeAll();
		this.add(health, BorderLayout.CENTER);
		this.validate();
		
	}
	
}
