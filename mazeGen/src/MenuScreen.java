import javax.swing.JFrame;

public class MenuScreen extends JFrame {

	private static final long serialVersionUID = 1L;
	private MenuView mv;
	
	MenuScreen() {
		this.setTitle("Koala Bomber");
		mv = new MenuView(400, 400*6/5);
		this.add(mv);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setFocusable(true);
		this.setLocationRelativeTo(null);
	}
	
	public void renderMenu() {
		this.setVisible(true);
	}
	
	public void hideMenu() {
		this.setVisible(false);
	}
	
}
