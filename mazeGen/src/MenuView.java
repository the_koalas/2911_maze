import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

public class MenuView extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private String background;
	
	MenuView(int width, int height) {
		this.background = "res/backdrop.png";	//Menu background
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setPreferredSize(new Dimension(width, height));
		this.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		createTitle();
		addButtons();
	}
	
	private void createTitle() {
		JLabel title = new JLabel ("<html><div style='background-color:transparent; width: 50px; height: 40px; '></div></html>");
		title.setAlignmentX(Component.CENTER_ALIGNMENT);
		this.add(title);
	}
	
	private void addButtons() {
		JButton play = createButton("PLAY");
		JButton tutorial = createButton("TUTORIAL");
		JButton quit = createButton("QUIT");
		
		play.setForeground(new Color(50, 99, 187));
		tutorial.setForeground(new Color(32, 178, 170));
		quit.setForeground(new Color(200, 25, 25));

		play.addActionListener(this);
		tutorial.addActionListener(this);
		quit.addActionListener(this);
		
		play.setActionCommand("play");
		tutorial.setActionCommand("tutorial");
		quit.setActionCommand("quit");
		
		this.add(play);
		insertGap();
		this.add(tutorial);
		insertGap();
		addDifficultyButtons();
		insertGap();
		this.add(quit);
	}
	
	private void addDifficultyButtons() {
		JRadioButton easy = createRadioButton("Easy", false);
		JRadioButton medium = createRadioButton("Medium", true);	// normal difficulty set as default
		JRadioButton hard = createRadioButton("Hard", false);
		easy.setForeground(new Color(46, 139, 87));
		medium.setForeground(new Color(65, 105, 225));
		hard.setForeground(new Color(165, 42, 42));
		
		ButtonGroup difficulty = new ButtonGroup();
		difficulty.add(easy);
		difficulty.add(medium);
		difficulty.add(hard);
		
		easy.addActionListener(this);
		medium.addActionListener(this);
		hard.addActionListener(this);
		
		easy.setActionCommand("setDifficultyEasy");
		medium.setActionCommand("setDifficultyMedium");
		hard.setActionCommand("setDifficultyHard");

		
		JLabel dt = new JLabel("Difficulty");
		dt.setFont(new Font("Calibri", Font.BOLD, 17));
		dt.setAlignmentX(Component.CENTER_ALIGNMENT);
		dt.setForeground(new Color(205, 92, 92));
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setMinimumSize(new Dimension(200, 120));
		buttonPanel.setPreferredSize(new Dimension(200, 120));
		buttonPanel.setMaximumSize(new Dimension(200, 120));
		buttonPanel.setBorder(new EmptyBorder(new Insets(10, 10, 10, 10)));
		buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		
		buttonPanel.add(dt);
		buttonPanel.add(easy);
		medium.setSelected(true);
		buttonPanel.add(medium);
		buttonPanel.add(hard);
		this.add(buttonPanel);
	}
	
	private JButton createButton(String name) {
		this.add(Box.createRigidArea(new Dimension(0, 15)));
		JButton btn = new JButton(name);
		btn.setAlignmentX(Component.CENTER_ALIGNMENT);
		btn.setMinimumSize(new Dimension(200, 55));
		btn.setPreferredSize(new Dimension(200, 55));
		btn.setMaximumSize(new Dimension(200, 55));
		btn.setFont(new Font("Calibri", Font.BOLD, 16));
		return btn;
	}
	
	private JRadioButton createRadioButton(String name, boolean isSelected) {
		JRadioButton rbtn = new JRadioButton(name, false);
		rbtn.setAlignmentX(Component.CENTER_ALIGNMENT);
		rbtn.setFont(new Font("Calibri", Font.PLAIN, 15));
		return rbtn;
	}
	
	private void insertGap() {
		this.add(Box.createRigidArea(new Dimension(0, 20)));
	}
	
	public Image loadImage(String src) {
		ImageIcon icon = new ImageIcon(src);
		Image img = icon.getImage();
		return img;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image background = loadImage(this.background); //Get the image referenced by the tile
		g.drawImage(background, 0, 0, null);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		switch(e.getActionCommand()) {
		case "setDifficultyEasy":
			GameEngine.difficulty = 0;
			break;
		case "setDifficultyMedium":
			GameEngine.difficulty = 1;
			break;
		case "setDifficultyHard":
			GameEngine.difficulty = 2;
			break;
		case "play":
			GameEngine.gameState = GameState.Start;
			break;
		case "tutorial":
			TutorialScreen ts = new TutorialScreen();
			ts.repaint();
			break;
		case "quit":
			System.exit(0);
			break;
		}
	}
	
}