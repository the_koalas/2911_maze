import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class MysteryTileSplash extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int bomb = 1;
	private final int health = 2;
	private final int reverse = 3;
	private final int kaboom = 4;
	private int height;
	private int width;
	private String background;
	private Color backgroundcolor = new Color(51,51,51);
	private int effect;
	
	/**
	 * Constructor for mysteryTileSplash. Sets the dimensions of the splash screen and the kind of effect that wants to be called.
	 * @param width
	 * @param height
	 * @param effect 1, 2, 3 and 4 for free bombs, free health, reverse direction and kaboom effect.
	 */
	public MysteryTileSplash(int width, int height, int effect){
		
		this.width = width;
		this.height = height;
		this.setPreferredSize(new Dimension(width, height));

		this.effect = effect;
		
		if (effect == bomb){
			
			
			background = "res/bombs.png";
			
			
		} else if (effect == health){
			
			background = "res/health.png";
			
			
		}else if (effect == reverse){
			
			background = "res/reverse.png";
			
			
			
		} else if (effect == kaboom) {
			background = "res/kaboom.png";
			
			
		}
		
		this.validate();
		
		
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		g.setColor(backgroundcolor);
		g.fillRect(0, 0, width, height);
		g.drawRect(0, 0, width, height);
		
		if (this.effect > 0){
			Image bg = loadImage(this.background); //Get the image referenced by the tile
			
			g.drawImage(bg, 0, 0, width, height, null);
			
		}
		
	}
	
	public Image loadImage(String src) {
		ImageIcon icon = new ImageIcon(src);
		Image img = icon.getImage();
		return img;
	}
	
}
