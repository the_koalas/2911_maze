
public class Player {
	private CoOrd position;
	private String imgSrc;
	private int size;
	private Direction direction;
	private int health;
	private int bombs;
	
	public Player(){
		
	}
	
	public Player(CoOrd position, String imgSrc, int size, Direction direction, int health, int bombs){
		this.position = position;
		this.imgSrc = imgSrc;
		this.size = size;
		this.direction = direction;
		this.health = health;
		this.bombs = bombs;
	}
	
	//========== Getters and Setters ==========

	public int getBombs() {
		return bombs;
	}

	public void setBombs(int bombs) {
		this.bombs = bombs;
	}
	
	public CoOrd getPosition() {
		return position;
	}

	public void setPosition(CoOrd position) {
		this.position = position;
	}

	public String getImgSrc() {
		return imgSrc;
	}

	public void setImgSrc(String imgSrc) {
		this.imgSrc = imgSrc;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}
	
	
	
}
