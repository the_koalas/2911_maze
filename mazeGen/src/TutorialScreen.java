import javax.swing.JFrame;

public class TutorialScreen extends JFrame {
	private static final long serialVersionUID = 1L;
	private TutorialView tv;
	
	TutorialScreen() {
		this.setTitle("Tutorial");
		tv = new TutorialView(400, 400*6/5, this);
		this.add(tv);
		this.pack();
		this.setFocusable(true);
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
		this.setVisible(true);
	}
}
