import java.util.Arrays;

public class Tile {

	private Integer[] wall = {1,1,1,1}; //{up, right, down, left}
	private CoOrd pos;
	private String imgSrc;
	private int tileSize;
	
	public Tile(CoOrd pos){
		this.pos = pos;
	}
	
	public Tile(CoOrd pos, String imgSrc){
		this.pos = pos;
		this.imgSrc = imgSrc;
	}
	
	public Tile(CoOrd pos, String imgSrc, int tileSize){
		this.pos = pos;
		this.imgSrc = imgSrc;
		this.tileSize = tileSize;
	}
	
	public Tile(CoOrd pos, String imgSrc, int tileSize, Integer[] wall){
		this.pos = pos;
		this.imgSrc = imgSrc;
		this.tileSize = tileSize;
		
		for (int i = 0; i < 4; i ++){
			this.wall[i] = wall[i];
		}
	}
	
	public int getTileSize(){
		return this.tileSize;
	}
	
	public void setTileSize(int tileSize){
		this.tileSize = tileSize;
	}
	
	public String getImgSrc(){
		return this.imgSrc;
	}
	
	public void setImageSrc(String imgSrc){
		this.imgSrc = imgSrc;
	}
	
	public void setUpWall(int x){
		wall[0] = x;
	}
	
	public void setRightWall(int x){
		wall[1] = x;
	}
	public void setDownWall(int x){
		wall[2] = x;
	}
	public void setLeftWall(int x){
		wall[3] = x;
	}
	
	public Integer[] getWall(){
		return wall;
	}
	
	public CoOrd getPosition(){
		return this.pos;
	}
	
	public void setPosition(CoOrd pos){
		this.pos = pos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((imgSrc == null) ? 0 : imgSrc.hashCode());
		result = prime * result + ((pos == null) ? 0 : pos.hashCode());
		result = prime * result + tileSize;
		result = prime * result + Arrays.hashCode(wall);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tile other = (Tile) obj;
		if (imgSrc == null) {
			if (other.imgSrc != null)
				return false;
		} else if (!imgSrc.equals(other.imgSrc))
			return false;
		if (pos == null) {
			if (other.pos != null)
				return false;
		} else if (!pos.equals(other.pos))
			return false;
		if (tileSize != other.tileSize)
			return false;
		if (!Arrays.equals(wall, other.wall))
			return false;
		return true;
	}
	

	
}
