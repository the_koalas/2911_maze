import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

class ArrowAction extends AbstractAction
{
    
	private static final long serialVersionUID = 1L;
	private String cmd;
    private GameEngine ge;
    
    public ArrowAction(String cmd, GameEngine ge) {
        this.cmd = cmd;
        this.ge = ge;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
    	
    
        if (cmd.equalsIgnoreCase("LeftArrow")) {
        	ge.arrowAction(cmd);
        	
        } else if (cmd.equalsIgnoreCase("RightArrow")) {
        	ge.arrowAction(cmd);
        } else if (cmd.equalsIgnoreCase("UpArrow")) {
        	ge.arrowAction(cmd);
        } else if (cmd.equalsIgnoreCase("DownArrow")) {
        	ge.arrowAction(cmd);
        }else if(cmd.equalsIgnoreCase("B")){
        	ge.arrowAction(cmd);
        }else if(cmd.equalsIgnoreCase("V")){
        	ge.arrowAction(cmd);
        }else if(cmd.equalsIgnoreCase("C")){
        	ge.arrowAction(cmd);
        }else if(cmd.equalsIgnoreCase("P")){
        	ge.arrowAction(cmd);
        }
    }
    
}
    