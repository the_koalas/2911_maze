import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GameSplash extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static Color BACKGROUNDCOLOR;
	private String background;
	private int height;
	private int width;
	
	public GameSplash(String label, String background, int height, int width) {
		this.height = height;
		this.width = width;
		this.background = background;	//Menu background
		this.setPreferredSize(new Dimension(width, height));
		JLabel msg = new JLabel("<html><p style='color:#9f6a3f; font-size:22px;'>" + label + "</p></html>");
		msg.setPreferredSize(new Dimension(350, 350));
		this.add(msg, BorderLayout.CENTER);
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		g.setColor(BACKGROUNDCOLOR);
		Image bg = loadImage(this.background); //Get the image referenced by the tile
		g.drawImage(bg, 0, 0, width, height, null);
		g.drawRect(0, 0, width, height);
		
	}
	public Image loadImage(String src) {
		ImageIcon icon = new ImageIcon(src);
		Image img = icon.getImage();
		return img;
	}
}