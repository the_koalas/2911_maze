
public class RunGame {
	public static void main(String[] args){

		int size = 20; //Set the N x N size of the game (In tiles not pixels)
		int tileSize = 40; //Set the size we want the tiles to be
		String tileSrc = "res/grass2.png"; //Set the image we want the tiles to use
		GameEngine ge = new GameEngine(size, tileSize, tileSrc);
		ge.start();
	}
}
		
