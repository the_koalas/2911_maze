UML

@startuml

class Player {
	-CoOrd position
	-String imgSrc
	-int size
	-Direction direction
	-int health
	-int bombs
	+Player()
	+Player(CoOrd position, String imgSrc, int size, Direction direction, int health, int bombs)
	+int getBombs()
	+void setBombs(int bombs)
	+CoOrd getPosition()
	+void setPosition(CoOrd position)
	+String getImgSrc()
	+void setImgSrc(String imgSrc)
	+int getSize()
	+void setSize(int size)
	+Direction getDirection()
	+void setDirection(Direction direction)
	+int getHealth()
	+void setHealth(int health)
}

class CoOrd {
	-int x
	-int y
	+CoOrd(int x, int y)
	+int getX()
	+int getY()
	+int hashCode()
	+boolean equals(Object obj)
	+String toString()
}

class Edge {
	-Node<E> head
	-Node<E> tail
	+Edge(Node<E> head, Node<E> tail)
	+Node<E> getHead()
	+Node<E> getTail()
	+Node<E> getConnection(Node<E> n)
	+String toString()
}

class GameEngine {
	-long serialVersionUID
	-boolean running
	-Thread thread
	-GameView gv
	~Grid gameGrid
	~Grid oldGameGrid
	-int featureSize
	-int showPathTimer
	-boolean reverseDirection
	-int reverseDirectionTimer
	-long levelCompleteTimer
	-int currentLevel
	-int goalY
	-int goalX
	-Node<Tile> goalNode
	-Tile[][] oldMap
	-int gameSize
	-int tileSize
	-String tileSrc
	-MenuScreen ms
	-SideMenu geSideMenu
	-Player player
	+int ELAPSEDTIME
	+int TIMELIMIT
	-int bombinit
	+int BOMBS
	+GameState gameState
	+ArrayList<int[]> mysteryTiles
	-long mysterySplashTimer
	+int mysterySplashType
	-MysteryTileSplash bombSplash
	-MysteryTileSplash healthSplash
	-MysteryTileSplash reverseSplash
	-MysteryTileSplash defaultSplash
	-MysteryTileSplash kaboomSplash
	-int timeStep
	+GameEngine(int size, int tileSize, String tileSrc)
	+void start()
	+void stop()
	+void initialise()
	-void initVariables(int difficulty)
	-void doFeature()
	-void render()
	-void setGoalNode()
	-void movePlayer(int key)
	+boolean checkMove(int key)
	+void arrowAction(String action)
	-boolean inRange(CoOrd pos)
	-void doBomb()
	+void addBindings()
	+void showPathTo(Node<Tile> from, Node<Tile> to, boolean show)
	+void run()
	-void timerEffect(long now)
	+int getGameSize()
	+int getTileSize()
	+String getTileSrc()
	+GameState getGameState()
	+void setGameState(GameState s)
	+Tile[][] replicateMap(Tile[][] oldMap)
}
class JFrame {
}
JFrame <|-- GameEngine
interface Runnable {
}
Runnable <|.. GameEngine

class GameScreen {
	+GameScreen()
}
class JFrame {
}
JFrame <|-- GameScreen

class GameView {
	-long serialVersionUID
	-Tile[][] tileMap
	-int tileSize
	-boolean mapReady
	-int width
	-int height
	-Player player
	-String playerSrc
	-int playerSize
	-int playerX
	-int playerY
	-boolean showPath
	-ArrayList<Tile> path
	+GameView(int width, int height, Player player)
	+void showPath(boolean show)
	+void loadTileMap(Tile[][] tiles)
	+void movePlayerToTile(int x, int y)
	+void drawPath(Graphics g)
	+void clearPath()
	+void addToPath(Tile t)
	+void drawWalls(Graphics g)
	+void drawPlayer(Graphics g)
	+void drawMap(Graphics g)
	+int translate(int value)
	+void paintComponent(Graphics g)
	+Image loadImage(String src)
	+Player getPlayer()
	+void setPlayer(Player player)
	+int getPlayerX()
	+void setPlayerX(int playerX)
	+int getPlayerY()
	+void setPlayerY(int playerY)
	+Tile[][] getTileMap()
	+void setTileMap(Tile[][] tileMap)
	+int getTileSize()
	+void setTileSize(int tileSize)
	+boolean isMapReady()
	+void setMapReady(boolean mapReady)
	+int getWidth()
	+void setWidth(int width)
	+int getHeight()
	+void setHeight(int height)
	+String getPlayerSrc()
	+void setPlayerSrc(String playerSrc)
	+int getPlayerSize()
	+void setPlayerSize(int playerSize)
}
abstract class JComponent {
}
JComponent <|-- GameView

interface Graph {
	void addNode(Node<E> n)
	void removeNode(Node<E> n)
	void addEdge(Edge<E> e)
	void removeEdge(Edge<E> e)
}


class GraphRep {
	-Hashtable<Node<E>,ArrayList<Node<E>>> graph
	+GraphRep()
	+void addNode(Node n)
	+void removeNode(Node n)
	+Node<E> getNode(E data)
	+void addEdge(Edge e)
	+void removeEdge(Edge e)
	+void Populate()
	+ArrayList<Node<E>> getNeighbours(Node<E> n)
	+void addToNeighbours(Node<E> subject, Node<E> nodeToAdd)
	+String toString()
}

Graph <|.. GraphRep

class Grid {
	-HashMap<Node<Tile>,ArrayList<Node<Tile>>> graph
	-HashMap<CoOrd,Node<Tile>> nodeRef
	-int tileSize
	-String tileSrc
	-int size
	-Node<Tile> lastVisited
	+Grid(int size, int tileSize, String tileSrc)
	+Grid(int size, int tileSize, String tileSrc, Tile[][] oldTile, Grid oldGrid)
	+int getGridSize()
	+Node<Tile> getNode(CoOrd pos)
	+Tile[][] getTile()
	+int getTileSize()
	+String getTileSrc()
	+ArrayList<Node<Tile>> getNeighbours(Node<Tile> n)
	+Node<Tile> getLastVisited()
	+void setLastVisited(Node<Tile> node)
	+void setTileSize(int tileSize)
	+void setTileSrc(String tileSrc)
	+void addNode(Node<Tile> n)
	+void addEdge(Edge<Tile> e)
	+void removeNode(Node<Tile> n)
	+void removeEdge(Edge<Tile> e)
	+void populateOldNodes(int size, Tile[][] oldTiles)
	+void Populate(int size)
	+void show()
	+void showBestMoves(CoOrd playerPos, CoOrd goal)
	+void getMysteryTileLocations(Node<Tile> goalNode, int numMysteryTiles)
	+void mazeGenBFS()
	+void breakWall(Node<Tile> from, Node<Tile> to, int commonWall)
	+int commonWall(Node<Tile> from, Node<Tile> to)
	-int randInt(int min, int max)
}
interface "Graph<Tile>" as Graph_Tile_ {
}
Graph_Tile_ <|.. Grid

Graph <|.. Grid


class MenuView {
	-long serialVersionUID
	-String background
	~MenuView(int width, int height)
	-void createMenu()
	+void paintComponent(Graphics g)
	-void createTitle()
	-void addButtons()
	-JButton createButton(String name)
	+Image loadImage(String src)
}
class JPanel {
}
JPanel <|-- MenuView


class Node {
	-ArrayList<Edge<E>> edges
	-E data
	-int id
	-Node<E> bestMove
	-Node<E> prev
	+Node(E data)
	+ArrayList<Edge<E>> getEdges()
	+E getData()
	+int getID()
	+Node<E> getBestMove()
	+Node<E> getPrev()
	+void setPrev(Node<E> prev)
	+void addEdge(Edge<E> edge)
	+void setBestMove(Node<E> node)
	+String toString()
	+boolean equals(Object o)
}

class RunGame {
	+void main(String[] args)
}

class Tile {
	-Integer[] wall
	-CoOrd pos
	-String imgSrc
	-int tileSize
	+Tile(CoOrd pos)
	+Tile(CoOrd pos, String imgSrc)
	+Tile(CoOrd pos, String imgSrc, int tileSize)
	+Tile(CoOrd pos, String imgSrc, int tileSize, Integer[] wall)
	+int getTileSize()
	+void setTileSize(int tileSize)
	+String getImgSrc()
	+void setImageSrc(String imgSrc)
	+void setUpWall(int x)
	+void setRightWall(int x)
	+void setDownWall(int x)
	+void setLeftWall(int x)
	+Integer[] getWall()
	+CoOrd getPosition()
	+void setPosition(CoOrd pos)
	+int hashCode()
	+boolean equals(Object obj)
}

enum Wall {
	UP
	RIGHT
	DOWN
	LEFT
	~Wall(int v)
}

class Bomb {
	-long serialVersionUID
	-JLabel bomb
	-int font_size
	-BufferedImage image
	-JLabel picture
	+Bomb()
	+void paintComponent(Graphics g)
}
class JPanel {
}

class Timer {
	-long serialVersionUID
	-JLabel timer
	+Timer()
	+void paintComponent(Graphics g)
}
class JPanel {
}

class SideMenu {
	-long serialVersionUID
	-int height
	-int width
	-Timer timer
	-Bomb bomb
	-Health health
	-GridBagConstraints gbc
	+Color BACKGROUNDCOLOR
	-JLabel splash
	-BufferedImage splashImage
	-JButton menu
	-JButton restart
	-JButton quit
	-JLabel title
	-GameEngine ge
	+SideMenu(int width, int height, GameEngine ge)
	+void render()
	+void paintComponent(Graphics g)
	+void actionPerformed(ActionEvent e)
	+Image loadImage(String src)
}
class JPanel {
}
JPanel <|-- SideMenu
interface ActionListener {
}
ActionListener <|.. SideMenu


class MysteryTileSplash {
	-int bomb
	-int health
	-int reverse
	-int kaboom
	-int height
	-int width
	-JLabel msg
	-String background
	-Color backgroundcolor
	-int effect
	+MysteryTileSplash(int width, int height, int effect)
	+void paintComponent(Graphics g)
	+Image loadImage(String src)
}
class JPanel {
}
JPanel <|-- MysteryTileSplash

class MenuScreen {
	-long serialVersionUID
	-MenuView mv
	~MenuScreen()
	+void renderMenu()
	+void hideMenu()
}
class JFrame {
}
JFrame <|-- MenuScreen

class Menu {
	-long serialVersionUID
	-JLabel title
	-JPanel menuPanel
	-int width
	-int height
	~Menu()
	-void createMenu()
	-void insertButton(String name, Container panel)
	+void renderMenu()
}
class JFrame {
}
JFrame <|-- Menu

interface DifficultyStrategy {
	void generateMaze(Grid g, Node<Tile> goal)
}
class EasyDifficulty {
	+void generateMaze(Grid g, Node<Tile> goal)
	-int randInt(int min, int max)
	-int manhattanDistance(Node<Tile> currNode, Node<Tile> goalNode)
}
interface DifficultyStrategy {
}
DifficultyStrategy <|.. EasyDifficulty


class MediumDifficulty {
	+void generateMaze(Grid g, Node<Tile> goal)
	-int randInt(int min, int max)
	-int manhattanDistance(Node<Tile> currNode, Node<Tile> goalNode)
}
interface DifficultyStrategy {
}
DifficultyStrategy <|.. MediumDifficulty

class MazeGenerator {
	-DifficultyStrategy difficulty
	+MazeGenerator(DifficultyStrategy strategy)
	+void generate(Grid g, Node<Tile> n)
}

class ArrowAction {
	-long serialVersionUID
	-String cmd
	-GameEngine ge
	+ArrowAction(String cmd, GameEngine ge)
	+void actionPerformed(ActionEvent e)
}
abstract class AbstractAction {
}
AbstractAction <|-- ArrowAction

class Health {
	-long serialVersionUID
	-JLabel health
	-float life
	-int width
	-int height
	+Health()
	+void paintComponent(Graphics g)
}
class JPanel {
}
JPanel <|-- Health

class HardDifficulty {
	+void generateMaze(Grid g, Node<Tile> goal)
	-int randInt(int min, int max)
	-int manhattanDistance(Node<Tile> currNode, Node<Tile> goalNode)
}
interface DifficultyStrategy {
}
DifficultyStrategy <|.. HardDifficulty


enum GameState {
	Start
	Menu
	Game
	Over
	Idle
	Restart
	NewGame
	LevelComplete
}


class GameSplash {
	-long serialVersionUID
	+Color BACKGROUNDCOLOR
	-String background
	-int height
	-int width
	+GameSplash(String label, String background, int height, int width)
	+void paintComponent(Graphics g)
	+Image loadImage(String src)
}
class JPanel {
}
JPanel <|-- GameSplash


enum Direction {
	UP
	DOWN
	LEFT
	RIGHT
	NO
}

JPanel <|-- Timer
JPanel <|-- Bomb

GameEngine o-- GameView
GameEngine o-- GameScreen
GameEngine o-- Grid
GameEngine -- RunGame

GraphRep *-- Node
GraphRep *-- Edge
Node o-- Edge

Grid *-- Tile
Tile *-- CoOrd

Tile -- Wall

Menu -- GameEngine
@enduml