import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Bomb extends JPanel{

	
	private static final long serialVersionUID = 1L;
	private JLabel bomb; 
	private int font_size = 18;
	private BufferedImage image;
	private JLabel picture;
	
	/**
	 * Constructor for bomb class extends JPanel.
	 * Displays bomb count on side menu during game.
	 */
	public Bomb(){
		
		bomb = new JLabel("<html><p style='color: #"+SideMenu.fontcolor+"; font-size: "+font_size+"px; font-weight:bold;'>"+GameEngine.BOMBS+"</p></html>");
		//add image
		
		try {
			image = ImageIO.read(new File("res/bomb.png"));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		Image dimg = image.getScaledInstance(30, 53, Image.SCALE_SMOOTH);
		
		picture = new JLabel();
		picture.setIcon(new ImageIcon(dimg));
		
		this.add(picture,  BorderLayout.WEST);
		this.add(bomb, BorderLayout.EAST);
		this.setOpaque(false);
	}
	
	/**
	 * Rewrites paint component for this class, so that the display will change as the number of bombs changes.
	 */
	public void paintComponent(Graphics g){
		
		g.setColor(SideMenu.BACKGROUNDCOLOR);
		
		
		
		g.drawRect(0, 0, getWidth(), getHeight());
		g.fillRect(0, 0, getWidth(), getHeight());
		
		bomb = new JLabel("<html><p style='color:#"+SideMenu.fontcolor+"; font-size: "+font_size+"px; font-weight:bold;'>"+GameEngine.BOMBS+"</p></html>");
		this.removeAll();
		this.add(picture,  BorderLayout.WEST);
		this.add(bomb, BorderLayout.EAST);
		this.validate();
	}
	
}
