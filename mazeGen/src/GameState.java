
public enum GameState {
	Start,
	Menu,
	Game,
	Over,
	Idle,
	Restart,
	NewGame,
	LevelComplete

}
