import java.awt.BorderLayout;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Timer extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel timer;
	
	/**
	 * Constructor for Timer class extends JPanel. Display Timer using timer constants created in GameEngine and colors defined in Sidemenu class.
	 */
	public Timer(){
		int elapsedTime = GameEngine.TIMELIMIT - GameEngine.ELAPSEDTIME;
		
		//Start timer on max time
		timer = new JLabel ("<html><p style='color:#"+SideMenu.fontcolor+"; text-align:center; font-size:15px; font-weight:bold; margin:10px 0 ;'>Timer</p><p style='color:#AA4D39; text-align:center; font-size:25px; margin-bottom:10px;'>"+elapsedTime+"</p></html>");
		this.add(timer, BorderLayout.CENTER);
	}
	
	public void paintComponent(Graphics g){
		
		g.setColor(SideMenu.BACKGROUNDCOLOR);
		g.drawRect(0, 0, getWidth(), getHeight());
		g.fillRect(0, 0, getWidth(), getHeight());
		int elapsedTime = GameEngine.TIMELIMIT - GameEngine.ELAPSEDTIME;
		String number = Integer.toString(elapsedTime);
		timer = new JLabel ("<html><p style='color:#"+SideMenu.fontcolor+"; text-align:center; font-size:15px; font-weight:bold; margin:10px 0 ;'>Timer</p><p style='color:#AA4D39; text-align:center; font-size:25px; margin-bottom:10px;'>"+number+"</p></html>");
		this.removeAll();
		this.add(timer, BorderLayout.CENTER);
		this.validate();
	}
	
}


