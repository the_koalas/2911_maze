import java.util.ArrayList;

public class Node<E> {
	private ArrayList<Edge<E>> edges = new ArrayList<Edge<E>>();
	private E data;
	private int id;
	private Node<E> bestMove = null;
	private Node<E> prev = null;
	
	public Node(E data){
		this.data = data;
	}
	
	public ArrayList<Edge<E>> getEdges(){
		return this.edges;
	}
	
	public E getData(){
		return this.data;
	}
	
	public int getID(){
		return this.id;
	}
	
	public Node<E> getBestMove() {
		return bestMove;
	}
	
	public Node<E> getPrev() {
		return this.prev;
	}
	
	public void setPrev(Node<E> prev) {
		this.prev = prev;
	}
	
	public void addEdge(Edge<E> edge) {
		this.edges.add(edge);
	}
	
	public void setBestMove(Node<E> node) {
		this.bestMove = node;
	}
	
	public String toString(){
		return "{data = " + data.toString() + ", edges = {" + this.edges.toString() + "}" + "}";
	}

	
	public boolean equals(Object o){
		if(o == this) return true;
		if(o == null) return false;
		if(o.getClass() != this.getClass()) return false;
		Node<?> other = (Node<?>)o;
		if(this.data.equals(other.data) && this.edges.equals(other.edges)){
			return true;
		}
		
		return false;
	}
}
