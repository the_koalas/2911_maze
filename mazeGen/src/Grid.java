
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;


public class Grid implements Graph<Tile>{

	private HashMap<Node<Tile>, ArrayList<Node<Tile>>> graph;
	//this map is for reference of nodes by its coordinates
	private HashMap<CoOrd, Node<Tile>> nodeRef;
	private int tileSize;
	private String tileSrc;

	//size of graph
	// size x size
	private int size;
	private Node<Tile> lastVisited;

	public Grid(int size, int tileSize, String tileSrc){
		this.nodeRef = new HashMap<CoOrd, Node<Tile>>();
		this.graph = new HashMap<Node<Tile>, ArrayList<Node<Tile>>>();
		this.size = size;
		this.tileSize = tileSize;
		this.tileSrc = tileSrc;
		Populate(size);
	}
	
	//initialize grid with giveen tiles
	public Grid(int size, int tileSize, String tileSrc, Tile[][] oldTile, Grid oldGrid){
		this.nodeRef = new HashMap<CoOrd, Node<Tile>>();
		this.graph = new HashMap<Node<Tile>, ArrayList<Node<Tile>>>();
		this.size = size;
		this.tileSize = tileSize;
		this.tileSrc = tileSrc;
		populateOldNodes(size, oldTile);
		
		
		//set best moves
		for (int i = 0; i < size; i ++){
			for (int j = 0; j < size; j ++){
				Node<Tile> newN = getNode(new CoOrd(i,j));
				Node<Tile> oldN = oldGrid.getNode(new CoOrd(i,j));
				if (oldN.getBestMove() != null){
					CoOrd next = oldN.getBestMove().getData().getPosition();
					Node<Tile> nextNode = getNode(new CoOrd(next.getX(),next.getY()));
					newN.setBestMove(nextNode);
					nextNode.setPrev(newN);
				}
			}
		}
		
	}
	
	public int getGridSize() {
		return this.size;
	}
	
	public Node<Tile> getNode(CoOrd pos){
		return nodeRef.get(pos);
	}

	/**
	 * Returns a two-dimensional array of tiles.
	 * @return
	 */
	public Tile[][] getTile(){

		if (size == 0){
			return null;
		}

		Tile[][] ret = new Tile[size][size];
		for (int i = 0; i < size; i ++){
			for (int j = 0; j < size; j ++){

				ret[i][j] = getNode(new CoOrd(i, j)).getData();

			}

		}

		return ret;

	}

	public int getTileSize(){
		return this.tileSize;
	}

	public String getTileSrc(){
		return this.tileSrc;
	}

	public ArrayList<Node<Tile>> getNeighbours(Node<Tile> n) {
		ArrayList<Node<Tile>> ret = new ArrayList<Node<Tile>>();
		for (Node<Tile> buf: graph.get(n)) {
			ret.add(buf);
		}
		return ret;
	}

	public Node<Tile> getLastVisited() {
		return this.lastVisited;
	}
	
	public void setLastVisited(Node<Tile> node) {
		this.lastVisited = node;
	}
	public void setTileSize(int tileSize){
		this.tileSize = tileSize;
	}

	public void setTileSrc(String tileSrc){
		this.tileSrc = tileSrc;
	}

	@Override
	public void addNode(Node<Tile> n) {
			//key: n the object,
			//value:list of edges connecting to n
			graph.put(n, new ArrayList<Node<Tile>>());
			//key: n's coOrd, value: n the object
			nodeRef.put(n.getData().getPosition(), n);

	}

	/**
	 *@pre: edge is valid in the sense
	 *that both the head and tail are contained
	 *in the graph
	 *@post: the tail of the edge will be in
	 *the head's neighbour list
	 */
	@Override
	public void addEdge(Edge<Tile> e) {
		graph.get(e.getHead()).add(e.getTail());
		graph.get(e.getTail()).add(e.getHead());
	}

	
	/**
	 * Populates a graph with predefined tiles
	 * @param size
	 */
	public void populateOldNodes(int size, Tile[][] oldTiles) {
		CoOrd newTilePos;
		Tile newTile;
		Node<Tile> nodeToAdd;

		//this blocks creates a square grid
		//of nodes without any edges
		for (int x_axs=0;x_axs<size;x_axs++) {
			for (int y_axs=0;y_axs<size;y_axs++) {
				newTilePos = new CoOrd(x_axs, y_axs);
				newTile = new Tile(newTilePos, this.tileSrc, this.tileSize, oldTiles[x_axs][y_axs].getWall());
				nodeToAdd = new Node<Tile>(newTile);
				addNode(nodeToAdd);
			}
		}

		//now we link all the nodes with edges
		int i = 0, j = 1;
		int x_axs = 0, y_axs = 0;

		Node<Tile> front;
		Node<Tile> behind;

		Edge<Tile> edgeToAdd;
		for(;x_axs<size;x_axs++) {
			for(;j<size;i++,j++) {
				front = getNode(new CoOrd(x_axs, j));
				behind = getNode(new CoOrd(x_axs, i));

				edgeToAdd = new Edge<Tile>(front, behind);
				addEdge(edgeToAdd);

			}
			//reset for next column
			//node with 'i' in its pos will always
			// be trailing node with j in it's pos
			i = 0;
			j = 1;
		}

		//exact same process as above but
		//now adding horizontal edges
		for(;y_axs<size;y_axs++) {
			for(;j<size;i++,j++) {
				front = getNode(new CoOrd(j, y_axs));
				behind = getNode(new CoOrd(i, y_axs));

				edgeToAdd = new Edge<Tile>(front, behind);
				addEdge(edgeToAdd);

			}
			//reset for next column
			//node with 'i' in its pos will always
			// be trailing node with j in it's pos
			i = 0;
			j = 1;
		}
	}

	/**
	 * makes size x size grid of nodes
	 * then places edges between each of them
	 * @param size
	 */
	public void Populate(int size) {
		CoOrd newTilePos;
		Tile newTile;
		Node<Tile> nodeToAdd;

		//this blocks creates a square grid
		//of nodes without any edges
		for (int x_axs=0;x_axs<size;x_axs++) {
			for (int y_axs=0;y_axs<size;y_axs++) {
				newTilePos = new CoOrd(x_axs, y_axs);
				newTile = new Tile(newTilePos);
				newTile.setImageSrc(this.tileSrc);
				newTile.setTileSize(this.tileSize);
				nodeToAdd = new Node<Tile>(newTile);

				addNode(nodeToAdd);
			}
		}

		//now we link all the nodes with edges
		int i = 0, j = 1;
		int x_axs = 0, y_axs = 0;

		Node<Tile> front;
		Node<Tile> behind;

		Edge<Tile> edgeToAdd;
		for(;x_axs<size;x_axs++) {
			for(;j<size;i++,j++) {
				front = getNode(new CoOrd(x_axs, j));
				behind = getNode(new CoOrd(x_axs, i));

				edgeToAdd = new Edge<Tile>(front, behind);
				addEdge(edgeToAdd);

			}
			//reset for next column
			//node with 'i' in its pos will always
			// be trailing node with j in it's pos
			i = 0;
			j = 1;
		}

		//exact same process as above but
		//now adding horizontal edges
		for(;y_axs<size;y_axs++) {
			for(;j<size;i++,j++) {
				front = getNode(new CoOrd(j, y_axs));
				behind = getNode(new CoOrd(i, y_axs));

				edgeToAdd = new Edge<Tile>(front, behind);
				addEdge(edgeToAdd);

			}
			//reset for next column
			//node with 'i' in its pos will always
			// be trailing node with j in it's pos
			i = 0;
			j = 1;
		}
	}

	//prints the graph row by row
	public void show(){
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i < size; i++){
			for(int j = 0; j < size; j++){

				Integer[] currWall = getNode(new CoOrd(i, j)).getData().getWall();

				sb.append("("+currWall[0]+","+currWall[1]+","+currWall[2]+","+currWall[3]+")");
			}
			sb.append("\n");
		}
	}


	public void showBestMoves(CoOrd playerPos, CoOrd goal) {
		Node<Tile> curr = getNode(playerPos);
		Node<Tile> goalNode = getNode(goal);
		while (curr != goalNode) {
			curr = curr.getBestMove();
		}
	}
	
	public ArrayList<CoOrd> getMysteryTileLocations( Node<Tile> goalNode, int numMysteryTiles) {
		Node<Tile> curr = getNode(new CoOrd(0, 0));
		ArrayList<Node<Tile>> pathToGoal = new ArrayList<Node<Tile>>();
		ArrayList<CoOrd> locations = new ArrayList<CoOrd>();
		CoOrd tileLocation = null;
	
		// first populate the pathToGoal array
		while(curr != goalNode) {
			pathToGoal.add(curr);
			curr = curr.getBestMove();
		}
		
		// now we find all the walkable forks off the path to goal
		ArrayList <Node<Tile>> goalPathForks = new ArrayList<Node<Tile>>();
		for (Node<Tile> n: pathToGoal) { 
			for (Integer wall: n.getData().getWall()) {
				// there is no wall
				if (wall == 0 ) {

					goalPathForks.add(n);
				}
			}
		}
		
		// number of walkable forks we actually found
		
		// if the number of forks is less than the number
		// of mystery tiles we want, make all fork a mysteryTIle
		int north = 0;
		int west = 1;
		int south = 2;
		int east = 3;
		
		int count = 0;
		while(count < numMysteryTiles) {
			if (count == goalPathForks.size()-1) {
				break;
			}
			Collections.shuffle(goalPathForks);
			
			Node<Tile> n = goalPathForks.get(count);
			int xPos = n.getData().getPosition().getX();
			int yPos = n.getData().getPosition().getY();
			int randWall = randInt(0, 3);
			
			Node<Tile> checkNode = null;
			for (int i=0; i<4; i++) {
				if (n.getData().getWall()[randWall] == 0) { // it doensn't have a wall
					if (randWall == north) {
						tileLocation = new CoOrd(xPos, yPos - 1);
						checkNode = getNode(tileLocation);
						if (!pathToGoal.contains(checkNode) && !locations.contains(tileLocation)) {
							locations.add(tileLocation);
							count++;
						}
						break;
					} else if (randWall == west) {
						tileLocation = new CoOrd(xPos + 1, yPos);
						checkNode = getNode(tileLocation);
						if (!pathToGoal.contains(checkNode) && !locations.contains(tileLocation)) {
							locations.add(tileLocation);
							count++;
						}
						break;
					} else if (randWall == south) {
						tileLocation = new CoOrd(xPos, yPos + 1);
						checkNode = getNode(tileLocation);
						if (!pathToGoal.contains(checkNode) && !locations.contains(tileLocation)) {
							locations.add(tileLocation);
							count++;
						}
						break;
					} else if (randWall == east) {
						tileLocation = new CoOrd(xPos - 1, yPos);
						checkNode = getNode(tileLocation);
						if (!pathToGoal.contains(checkNode) && !locations.contains(tileLocation)) {
							locations.add(tileLocation);
							count++;
						}
						break;
					}
				}
				randWall = (randWall+1)%4;
			}
			if (locations.contains(goalNode.getData().getPosition())) {
				locations.remove(goalNode.getData().getPosition());
				count--;
			}
		}
		
		return locations;
		
	}

	
	public void breakWall(Node<Tile> from, Node<Tile> to, int commonWall) {
		int north = 1, south = 3;
		int east = 2, west = 4;

		if (commonWall == north){
			//if going up
			//break top wall of currNode
			//break bottom wall of nextNode
			from.getData().setUpWall(0);
			to.getData().setDownWall(0);
		} else if (commonWall == east){
			//if going right
			//break right wall of currNode
			//break left wall of nextNode
			from.getData().setRightWall(0);
			to.getData().setLeftWall(0);
		} else if (commonWall == south){
			//if going down
			//break bottom wall of currNode
			//break top wall of nextNode

			from.getData().setDownWall(0);
			to.getData().setUpWall(0);
		} else if (commonWall == west){
			//if going left
			//break left wall of currNode
			//break right wall of nextNode
			from.getData().setLeftWall(0);
			to.getData().setRightWall(0);

		}

	}

	/**
	 * Takes in 2 nodes and returns a direction based on its coordinates.
	 *
	 *
	 * @param from
	 * @param to
	 * @return 1 = up, 2 = right, 3 = down, 4 = left, -1 is error
	 */
	public int commonWall(Node<Tile> from, Node<Tile> to){
		int ret = -1;

		int x1 = from.getData().getPosition().getX();
		int y1 = from.getData().getPosition().getY();
		int x2 = to.getData().getPosition().getX();
		int y2 = to.getData().getPosition().getY();

		if (x2 > x1 && y2 == y1){
			ret = 2;
		} else if(x2 < x1 && y2 == y1){
			ret = 4;
		} else if (x2 == x1 && y2 < y1){
			ret = 1;
		} else if (x2 == x1 && y2 > y1){
			ret = 3;
		}


		return ret;
	}

	private int randInt(int min, int max) {
	
	    // Usually this can be a field rather than a method variable
		Random rand = new Random();
	
	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	
	    return randomNum;
	}
}
