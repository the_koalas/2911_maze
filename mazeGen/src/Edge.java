
public class Edge<E> {
	private Node<E> head;
	private Node<E> tail;
	
	
	public Edge(Node<E> head, Node<E> tail){
		this.head = head;
		this.tail = tail;
	}
	
	public Node<E> getHead(){
		return this.head;
	}
	
	public Node<E> getTail(){
		return this.tail;
	}	
	/**
	 * @pre: 'n' must be either the head or the tail
	 * @post: returns the other node which is 
	 * connected by @param n's edge
	 */
	public Node<E> getConnection(Node<E> n) {
		if (n.equals(head)) {
			return this.tail;
		}else {
			return this.head;
		}
	}
	
	public String toString(){
		return head.getData().toString() + " <---> " + tail.getData().toString();
	}
}
