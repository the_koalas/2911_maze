
public class MazeGenerator {
	private DifficultyStrategy difficulty;
	
	public MazeGenerator(DifficultyStrategy strategy) {
		this.difficulty = strategy;
	}
	
	public void generate(Grid g, Node<Tile> n) {
		difficulty.generateMaze(g, n);
	}
}
