
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.KeyStroke;


public class GameEngine extends JFrame implements Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean running = false;
	private boolean gameIsOver = false;
	private boolean levelIsComplete = false;
	private Thread thread;

	
	private GameView gv;
	Grid gameGrid;
	Grid oldGameGrid;
	
	private static final int easyMode = 0;
	private static final int mediumMode = 1;
	private static final int hardMode = 2;
	public static int difficulty = mediumMode;//make default easy mode?
	private int goalY;
	private int goalX;
	private Node<Tile> goalNode;
	private Tile[][] oldMap;
	private int gameSize;
	private int tileSize;
	private String tileSrc;
	private MenuScreen ms = new MenuScreen();
	private SideMenu geSideMenu;
	private Player player = null;
	public static int ELAPSEDTIME = 0;
	public static int TIMELIMIT = 60;
	private int bombinit = 3;
	public static int BOMBS = 0;
	public static GameState gameState = GameState.Menu;
	private JComponent go = null; 
	
	//feature set variables
	private int featureSize = 3;
	private int showPathTimer = 0;
	private boolean showPath = false;
	private boolean reverseDirection = false;
	private int reverseDirectionTimer = 0;
	private long levelCompleteTimer = 0;
	private int currentLevel = 1;
		
	//For mystery tiles and splash
	public static ArrayList<CoOrd> mysteryTiles;
	private long mysterySplashTimer = 0;
	public static int mysterySplashType = 0;
	private MysteryTileSplash bombSplash = null;
	private MysteryTileSplash healthSplash = null;
	private MysteryTileSplash reverseSplash = null;
	private MysteryTileSplash kaboomSplash = null;
	private final int timeStep = 1000;
	
	public GameEngine(int size, int tileSize, String tileSrc){
		this.gameSize = size;
		this.tileSize = tileSize;
		this.tileSrc = tileSrc;
		
		this.setTitle("Tiles!"); //Set a title
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Close on close, makes sense
		this.pack(); //Re-size the JFrame such that it perfectly fits all of its components.
		this.setFocusable(true);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/5, dim.height/2-this.getSize().height*10);
	}

	// ======== Game Engine Start/Stop conditions ====
	public synchronized void start() {
		if (running) {
			return; //already running, do nothing
		}
		running = true;
		thread = new Thread(this);
		thread.start(); //calls run
	}
	
	public synchronized void stop() {
		if (!running) {
			return; //already not running, do nothing
		}
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	// ==============================================
	
	public void initialise(){

		gameGrid = new Grid(gameSize, tileSize, tileSrc); //Create our graph
		

	}
	
	/**
	 * initializes common variable such as new player, timer, health and bombs.
	 */
	private void initVariables(){
		BOMBS = bombinit; //has to be before sm is created
		//reset player
		this.player = new Player();
		player.setDirection(Direction.NO);
		player.setPosition(new CoOrd(0, 0));
		player.setHealth(100);
		player.setImgSrc("res/player.gif");
		player.setSize((int) (this.tileSize * 0.75));
		player.setBombs(BOMBS);
		ELAPSEDTIME = 0; //reset timer
		showPathTimer = 0;
		reverseDirection = false;
		reverseDirectionTimer = 0;
		
		
	}
	
	/**
	 * Gets called when a player's location is on a mystery tile location. This function will randomly pick an effect/feature to be implemented on the player.
	 * Effects are, adding bombs, adding health or reverse controls for 10 seconds.
	 * This method resets the mysterySplashTimer to current time whenever it is called and adds 2 more seconds. 
	 * MysterySplashType will be set to a constant that represents the type of effect called. Type 1, 2 and 3 represents add bombs, add health and reverse directions respectively. 
	 */
	private void doFeature(){
		Random r = new Random();
		int x = r.nextInt(featureSize);
		
		//get currentTime
		mysterySplashTimer = System.currentTimeMillis() + timeStep * 2;
		mysterySplashType = x + 1;
		
		switch(x){
			
			case 0:
				//add bomb
				GameEngine.BOMBS = player.getBombs() + 2;
				player.setBombs(GameEngine.BOMBS);
				this.geSideMenu.repaint();
				
				break;
			
			case 1:
				//add health
				int percent = GameEngine.TIMELIMIT/10;
				if (GameEngine.ELAPSEDTIME > percent)
					GameEngine.ELAPSEDTIME -= percent;
				else 
					GameEngine.ELAPSEDTIME = 0;
				
				break;
				
			case 2:
				//reverse direction
				this.reverseDirection = true;
				this.reverseDirectionTimer += 10;
				break;
		
		}
	}
	
	/**
	 * Takes in a difficulty level and initializes the game accordingly. 
	 * @param difficulty
	 */
	private void initializeLevel(int difficulty){
		//dispose menuScreen
		ms.dispose();
		
		if (difficulty == GameEngine.easyMode){
			this.gameSize = 16;
			this.tileSize = 50;
			
		} else if (difficulty == GameEngine.mediumMode){
			this.gameSize = 20;
			this.tileSize = 40;
			
		} else if (difficulty == GameEngine.hardMode){
			this.gameSize = 23;
			this.tileSize = 35;
			
		}
		
		initialise();
		
		initVariables();
		int menuWidth = Math.max((gameSize * gameGrid.getTileSize()/5), 350);
		int menuHeight = gameSize * gameGrid.getTileSize();
		SideMenu sm = new SideMenu(menuWidth, menuHeight, this);
		
		//set up splash
		bombSplash = new MysteryTileSplash(menuWidth, menuHeight, 1);
		healthSplash = new MysteryTileSplash(menuWidth, menuHeight, 2);
		reverseSplash = new MysteryTileSplash(menuWidth, menuHeight, 3);
		kaboomSplash = new MysteryTileSplash(menuWidth, menuHeight, 4);
		
		kaboomSplash.setPreferredSize(new Dimension(menuWidth, menuHeight));
		bombSplash.setPreferredSize(new Dimension(menuWidth, menuHeight));
		healthSplash.setPreferredSize(new Dimension(menuWidth, menuHeight));
		reverseSplash.setPreferredSize(new Dimension(menuWidth, menuHeight));
		sm.setPreferredSize(new Dimension(menuWidth, menuHeight));
		
		setGoalNode();
		
		MazeGenerator mg = null;
		if (difficulty == GameEngine.easyMode){
			mg = new MazeGenerator(new EasyDifficulty());
			
		} else if (difficulty == mediumMode){
			mg = new MazeGenerator(new MediumDifficulty());
			
		} else if (difficulty == hardMode){
			mg = new MazeGenerator(new HardDifficulty());
			
		}
		
		
		mg.generate(this.gameGrid, goalNode);

		
		//stores to mysteryTile, to be called in GameView to draw mystery tiles.
		GameEngine.mysteryTiles = this.gameGrid.getMysteryTileLocations(goalNode, (difficulty * difficulty)+1);
		
		
		Tile[][] tempMap = gameGrid.getTile(); //Get the Tile 2D array that represents our graph
		
		//replicate map and grid for case of restart
		oldMap = replicateMap(tempMap);
		oldGameGrid = new Grid(gameSize, tileSize, tileSrc, oldMap, gameGrid);
		
		gv = new GameView(gameSize * gameGrid.getTileSize(), gameSize * gameGrid.getTileSize(), player); //Set the size of the game view
		gv.loadTileMap(tempMap); //Load our map into the GameView
		
		addBindings();
		this.add(gv, BorderLayout.CENTER); //Add the GameView to our JFrame
		//this.add(splash, BorderLayout.EAST);
		this.add(sm, BorderLayout.EAST);
		
		
		this.pack(); //Pack the JFrame so that it fits our view perfectly
		this.revalidate();
		this.setVisible(true);
		
		gv.loadTileMap(tempMap); //Load our map into the GameView
		gv.movePlayerToTile(player.getPosition().getX(), player.getPosition().getY()); // Move the player (Using tiles x,y. Not pixels)

		this.geSideMenu = sm; //save SM for future render loops
		
		gameState = GameState.Game;
	}
	
	
	/**
	 * Render method is called in game loop every nanosecond, checks the game state and updates the JFrame accordingly.
	 * The following describes the effects for each game state.
	 * GameState.Start a new maze game will be generated. 
	 * GameState.Restart the same maze generated from before will be regenerated.
	 * GameState.Game means a game is in progress, thus it checks if the game is over or a mystery tile has been triggered.
	 * GameState.Menu means the game is not being played and displays the main menu.
	 * GameState.LevelComplete represents the transition between winning a game and loading a new game, thus displaying the winning game splash screen.
	 * GameState.newGame initializes a new maze and transitions itself to GameState.Start.
	 * 
	 * 
	 */
	private void render(){
		//Start case runs if Play button is pressed on MenuScreen
		if (gameState == GameState.Start){
			
			initializeLevel(GameEngine.difficulty);

		} else if (gameState == GameState.Game){
			// ========= Game Over Screen Logic ============
			// If SM shows 0 time, remove maze and put in Game Over Screen
			// Time left = time limit - time running

			if (GameEngine.TIMELIMIT - GameEngine.ELAPSEDTIME  <= 0 && gameIsOver == false) {
				gameIsOver = true;
				go = new GameSplash("", "res/gameover.png",
												gameSize * gameGrid.getTileSize(), gameSize * gameGrid.getTileSize());
				go.setPreferredSize(new Dimension(gameSize * gameGrid.getTileSize(), gameSize * gameGrid.getTileSize()));
				this.getContentPane().removeAll();
				this.getContentPane().add(go, BorderLayout.CENTER);
				this.getContentPane().add(geSideMenu, BorderLayout.EAST);

				this.validate();
				this.pack();
				this.repaint();


			}

			// Remove mystery tile from mysteryTile after player steps on one.
			for (int i = 0; i < mysteryTiles.size(); i ++){
				CoOrd coord = mysteryTiles.get(i);
				if (player.getPosition().getX() == coord.getX() && player.getPosition().getY() == coord.getY()){
					doFeature();
					mysteryTiles.remove(coord);
					i --;
					if (mysteryTiles.isEmpty())
						break;
					
				}
			}
			

		} else if (gameState == GameState.Menu){

			if (gv == null) {
				
			}
			else {
				//to reset jframe and load new jpanels if needed
				gv = null;
				gameIsOver = false;
				this.getContentPane().removeAll();
				this.dispose();
			}
			ms.renderMenu();


		} else if (gameState == GameState.NewGame){
			if (gv != null){
				//to reset jframe and load new jpanels
				gv = null;
				gameIsOver = false;
				geSideMenu.removeAll();
				this.getContentPane().removeAll();
				this.dispose();
				
			
			}
			gameState = GameState.Start;
		
		}else if (gameState == GameState.Restart){
			//restart game
			if (gv != null){
				//to reset jframe and load new jpanels
				gv = null;
				gameIsOver = false;
				this.currentLevel = 1;
				geSideMenu.removeAll();
				this.getContentPane().removeAll();
				this.dispose();
				
			
			}
			
			//reset map, grid and goal node.
			Tile[][] tempMap = replicateMap(oldMap);
			gameGrid = new Grid(gameSize, tileSize, tileSrc, tempMap, gameGrid);
			setGoalNode();
			gameGrid.show(); //Show the maze in text
			GameEngine.mysteryTiles = this.gameGrid.getMysteryTileLocations(goalNode, (difficulty * difficulty) + 1);
			int menuWidth = Math.max((gameSize * gameGrid.getTileSize()/5), 350);
			int menuHeight = gameSize * gameGrid.getTileSize();
			
			
			SideMenu sm = new SideMenu(menuWidth, menuHeight, this);
			sm.setPreferredSize(new Dimension(menuWidth, menuHeight));
			//set up splash
			bombSplash = new MysteryTileSplash(menuWidth, menuHeight, 1);
			healthSplash = new MysteryTileSplash(menuWidth, menuHeight, 2);
			reverseSplash = new MysteryTileSplash(menuWidth, menuHeight, 3);
			kaboomSplash = new MysteryTileSplash(menuWidth, menuHeight, 4);
			
			kaboomSplash.setPreferredSize(new Dimension(menuWidth, menuHeight));
			bombSplash.setPreferredSize(new Dimension(menuWidth, menuHeight));
			healthSplash.setPreferredSize(new Dimension(menuWidth, menuHeight));
			reverseSplash.setPreferredSize(new Dimension(menuWidth, menuHeight));
			
			
			initVariables();
			
			gv = new GameView(gameSize * gameGrid.getTileSize(), gameSize * gameGrid.getTileSize(), player); //Set the size of the game view
			gv.loadTileMap(oldMap); //Load our map into the GameView
			
			addBindings();
				this.add(gv, BorderLayout.CENTER); //Add the GameView to our JFrame
				this.add(sm, BorderLayout.EAST);
			this.pack(); //Pack the JFrame so that it fits our view perfectly
			
			
			this.revalidate();
			this.setVisible(true);
			this.geSideMenu = sm; //save SM for future render loops
			
			gameState = GameState.Game;
			
		} else if (gameState == GameState.LevelComplete){
				if (levelIsComplete == false) {
					levelIsComplete = true;
					levelCompleteTimer = System.currentTimeMillis() + 2000; //2 second splash screen
					this.getContentPane().removeAll();
					JComponent lc = new GameSplash("Level " + this.currentLevel + " Complete", "res/backgroundScreen.png",
													gameSize * gameGrid.getTileSize(), gameSize * gameGrid.getTileSize());
					lc.add(new JLabel("<html><p style='color:#9f6a3f; font-size:22px;'>Goal Reached: Home Sweet Home! </p></html>"));
					this.getContentPane().add(lc, BorderLayout.CENTER);
					this.getContentPane().add(geSideMenu, BorderLayout.EAST);

					this.validate();
					this.pack();
					this.repaint();
				}


		} else if (gameState == GameState.Idle){

		}

	}

	private void setGoalNode(){ //where the goal square is placed.
		
		
		goalX = this.gameSize - 1;
		goalY = this.gameSize - 1;
		
		goalNode = gameGrid.getNode(new CoOrd(goalX, goalY));
		
		gameGrid.getNode(new CoOrd(goalX, goalY)).getData().setImageSrc("res/goal.png");
	}

	private void movePlayer(int key){
		int playerX = this.player.getPosition().getX();
		int playerY = this.player.getPosition().getY();
		switch(key){
		case KeyEvent.VK_UP:
			playerY--;
       	 	gv.movePlayerToTile(playerX, playerY);//game view handles the actual moving
			break;

		case KeyEvent.VK_DOWN:

       	 	playerY++;
       	 	gv.movePlayerToTile(playerX, playerY);
			break;

		case KeyEvent.VK_LEFT:
			playerX--;
       	 	gv.movePlayerToTile(playerX, playerY);
			break;

		case KeyEvent.VK_RIGHT:
			playerX++;
       	 	gv.movePlayerToTile(playerX, playerY);
			break;
		}
		
		player.getPosition().setX(playerX);
		player.getPosition().setY(playerY);
		gameGrid.showBestMoves(player.getPosition(), new CoOrd(goalX, goalY));
		if(showPathTimer > 0)
		this.showPathTo(gameGrid.getNode(new CoOrd(playerX, playerY)), gameGrid.getNode(new CoOrd(goalX, goalY)), true);
		else this.gv.showPath(false);
		
		if(player.getPosition().equals(new CoOrd(goalX, goalY))){
			
			//newGame
			gameState = GameState.LevelComplete;
		}
	}
	//checks if move is legal
	public boolean checkMove(int key){
		Tile tempTile = gameGrid.getNode(player.getPosition()).getData();
		Integer[] walls = tempTile.getWall();
		
		int playerX = this.player.getPosition().getX();
		int playerY = this.player.getPosition().getY();
		
		switch(key){
		case KeyEvent.VK_UP:
			if(!inRange(new CoOrd(playerX, playerY - 1))) return false;
			if(walls[0] == 1) return false;
			if(gameGrid.getNode(new CoOrd(playerX, playerY - 1)).getData().getWall()[2] == 1) return false;
			break;

		case KeyEvent.VK_DOWN:
			if(!inRange(new CoOrd(playerX, playerY + 1))) return false;
			if(walls[2] == 1) return false;
			if(gameGrid.getNode(new CoOrd(playerX, playerY + 1)).getData().getWall()[0] == 1) return false;
			break;

		case KeyEvent.VK_LEFT:
			if(walls[3] == 1) return false;
			if(gameGrid.getNode(new CoOrd(playerX - 1, playerY)).getData().getWall()[1] == 1) return false;
			break;

		case KeyEvent.VK_RIGHT:
			if(!inRange(new CoOrd(playerX + 1, playerY))) return false;
			if(walls[1] == 1) return false;
			if(gameGrid.getNode(new CoOrd(playerX + 1, playerY)).getData().getWall()[3] == 1) return false;
			break;
		}

		return true;
	}

	/**
	 * 
	 * @param action
	 */
	public void arrowAction(String action){
		Direction temp = player.getDirection();
		
		if (this.reverseDirection == true){
			switch(action){
				case "UpArrow":
					action = "DownArrow";
					break;
				case "DownArrow":
					action = "UpArrow";
					break;
				case "LeftArrow":
					action = "RightArrow";
					break;
				case "RightArrow":
					action = "LeftArrow";
					break;
			}
		}
		
		
		switch(action){
        case "UpArrow":
			 player.setDirection(Direction.UP);
	       	 if(checkMove(KeyEvent.VK_UP)){
		        	 movePlayer(KeyEvent.VK_UP); //actually does the moving
	       	 }
       	 break;

    	case "DownArrow":
    		 player.setDirection(Direction.DOWN);
    		 if(checkMove(KeyEvent.VK_DOWN)){
    			 movePlayer(KeyEvent.VK_DOWN);
    		 }
       	 break;

    	case "LeftArrow":
    		 player.setDirection(Direction.LEFT);
    		 if(checkMove(KeyEvent.VK_LEFT)){
        		 movePlayer(KeyEvent.VK_LEFT);
    		 }
       	 break;

    	case "RightArrow":
    		 player.setDirection(Direction.RIGHT);
    		 if(checkMove(KeyEvent.VK_RIGHT)){
	        	 movePlayer(KeyEvent.VK_RIGHT);
    		 }
       	 break;
       	
    	case "B":
    		doBomb();
    		//set number of bombs
    		BOMBS = player.getBombs();
    		
    		
    	break;
    	
    	case "V":
    		//illuminate:
    		if (showPathTimer > 0){
    			
    		} else {
	    		showPathTimer += 2;
	    		showPath = true;
	    		this.showPathTo(this.gameGrid.getNode(player.getPosition()), this.goalNode, true);
    		}
    		break;
		 
    	case "C":

		 break;
		 
    	case "P":
    		
		 break;
		
        }
		if(player!=null && !temp.equals(player.getDirection())) gv.repaint();	


    }
	
	private boolean inRange(CoOrd pos){
		if(pos.getX() >= 0 && pos.getX() < this.gameSize){
			if(pos.getY() >= 0 && pos.getY() < this.gameSize){
				return true;
			}
		}
		return false;
	}
	
	private void doBomb(){
		
		
		if(this.player.getBombs() <= 0){ //Check if we have bombs
			return;
		}
		

		
		Tile temp = gameGrid.getNode(player.getPosition()).getData(); //Get the tile of interest
		
		
		int playerX = this.player.getPosition().getX(); //Get x and y
		int playerY = this.player.getPosition().getY();
		
		switch(this.player.getDirection()){
		case UP: //Destroy UP wall
			if(temp.getWall()[0] == 1 && playerY != 0){ //If there is a wall (We don't want to waste the player's bombs)
				temp.setUpWall(0); //Destroy that wall
				if(inRange(new CoOrd(playerX, playerY - 1))){ //Check if the opposing wall falls on a tile on our screen
					Integer[] otherWall = gameGrid.getNode(new CoOrd(playerX, playerY - 1)).getData().getWall(); //Get that wall
					otherWall[2] = 0; //Also destroy it
				}
				player.setBombs(player.getBombs() - 1); // Reduce the bombs
				//bomb splash
				mysterySplashTimer = System.currentTimeMillis() + timeStep;
				mysterySplashType = 4;
			}
			break;
			
		case RIGHT: //Same as up, slight tweaks
			if(temp.getWall()[1] == 1 && playerX != this.gameSize-1){
				temp.setRightWall(0);
				if(inRange(new CoOrd(playerX + 1, playerY))){
					Integer[] otherWall = gameGrid.getNode(new CoOrd(playerX + 1, playerY)).getData().getWall();
					otherWall[3] = 0;
				}
				player.setBombs(player.getBombs() - 1);
				//bomb splash
				mysterySplashTimer = System.currentTimeMillis() + timeStep;
				mysterySplashType = 4;
			}
			break;
		
		case DOWN: //Same as up, slight tweaks
			if(temp.getWall()[2] == 1 && playerY != this.gameSize-1){
				temp.setDownWall(0);
				if(inRange(new CoOrd(playerX, playerY + 1))){
					Integer[] otherWall = gameGrid.getNode(new CoOrd(playerX, playerY + 1)).getData().getWall();
					otherWall[0] = 0;
				}
				player.setBombs(player.getBombs() - 1);
				//bomb splash
				mysterySplashTimer = System.currentTimeMillis() + timeStep;
				mysterySplashType = 4;
			}
			break;
			
		case LEFT: //Same as up, slight tweaks
			if(temp.getWall()[3] == 1 && playerX != 0){
				temp.setLeftWall(0);
				if(inRange(new CoOrd(playerX - 1, playerY))){
					Integer[] otherWall = gameGrid.getNode(new CoOrd(playerX - 1, playerY)).getData().getWall();
					otherWall[1] = 0;
				}
				player.setBombs(player.getBombs() - 1);
				//bomb splash
				mysterySplashTimer = System.currentTimeMillis() + timeStep;
				mysterySplashType = 4;
			}
			break;
			
		case NO:
			break;
			
		default:
			break;
		}
		
		Tile[][] tempMap = gameGrid.getTile(); //Get the 2D array of tiles again as we need to re-load them into GameView
		gv.loadTileMap(tempMap); //Load the tiles
		gv.repaint(); //Repaint the screen
		
	}
	

	public void addBindings(){
		InputMap im = gv.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
	    ActionMap am = gv.getActionMap();
	
	    im.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "RightArrow");
	    im.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "LeftArrow");
	    im.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "UpArrow");
	    im.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "DownArrow");
	    im.put(KeyStroke.getKeyStroke(KeyEvent.VK_B, 0), "B");
	    im.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, 0), "V");
	    im.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, 0), "C");
	    im.put(KeyStroke.getKeyStroke(KeyEvent.VK_P, 0), "P");
	
	    am.put("RightArrow", new ArrowAction("RightArrow", this));
	    am.put("LeftArrow", new ArrowAction("LeftArrow", this));
	    am.put("UpArrow", new ArrowAction("UpArrow", this));
	    am.put("DownArrow", new ArrowAction("DownArrow", this));
	    am.put("B", new ArrowAction("B", this)); //Bomb button
	    am.put("V", new ArrowAction("V", this)); //View best path button
	    am.put("C", new ArrowAction("C", this));
	    am.put("P", new ArrowAction("P", this));
	}
	
	public void showPathTo(Node<Tile> from, Node<Tile> to, boolean show){
		Node<Tile> temp = from;
		gv.clearPath();
		while(!temp.equals(to)){
			Tile t = temp.getData();

			gv.addToPath(t);
			temp = temp.getBestMove();
		}

		gv.showPath(show);
		gv.repaint();
	}
	
	

	@Override
	public void run() {
		//calculate FPS
		long lastTime = System.nanoTime(); //take current time in nanoseconds
		final double amountOfTicks = 60.0; //we want 60 'ticks'/frames per second
		double ns = 1000000000 / amountOfTicks; // 1 nanosecond = 1e-9 seconds
		double delta = 0; //change in time to catch up
		long timer = System.currentTimeMillis();
		ELAPSEDTIME = 0;
		while (running) { //see start - check if game is running
			long now = System.nanoTime();
			delta += (now - lastTime) / (ns);
			lastTime = now;
			if (delta >= 1) {
				//do stuff needing updates every second`
				render();
				delta--;
			}
			
			//draw screen render();
			
			//below updates every second - how many frames in the second
			if (System.currentTimeMillis() - timer > 1000) { //if more than 1000ms (1s) passed, print calc
				if (GameEngine.TIMELIMIT - GameEngine.ELAPSEDTIME > 0 && !(GameEngine.gameState == GameState.LevelComplete)) {
					GameEngine.ELAPSEDTIME ++;
				}
				
				timer += 1000;
				
				//timer for game features
				if (GameEngine.gameState == GameState.Game){
					timerEffect(timer);
				}
			} else if (levelCompleteTimer != 0 && System.currentTimeMillis() - levelCompleteTimer > 0) {
				levelIsComplete = false; //works without this.. delete?
				this.currentLevel++;
				levelCompleteTimer = 0;
				GameEngine.gameState = GameState.NewGame;
			}

		}
		stop();
	}
	
	/**
	 * Should only be called every second when GameState = GameState.Game.
	 * Does not check for these constraints to be true.
	 * Adjust timers of mystery tile effects, such as timer for reverseDirection effect and splash screen.
	 */
	private void timerEffect(long now){
		if (reverseDirectionTimer > 0){
			this.reverseDirectionTimer -= 1;
		} else {
			this.reverseDirection = false;
			this.reverseDirectionTimer = 0;
		}
		
		if (showPathTimer > 0){
			
			showPathTimer -= 1;
			this.showPathTo(this.gameGrid.getNode(player.getPosition()), this.goalNode, showPath);
			//each second decrease life
			long penalty = GameEngine.TIMELIMIT/20;
			if (GameEngine.TIMELIMIT - GameEngine.ELAPSEDTIME <= penalty){
				GameEngine.ELAPSEDTIME  = GameEngine.TIMELIMIT;
			} else {
				GameEngine.ELAPSEDTIME += penalty;
			}
			
			
		} else if (showPath == true && showPathTimer < 1){
			//stop illuminating
			showPath = false;
			this.showPathTo(this.gameGrid.getNode(player.getPosition()), this.goalNode, showPath);
		}
		
		//turns mysterySplash off if timer goes to 0, else decreases timer for mystery splash.
		
		
		if (mysterySplashTimer > now){
			mysterySplashTimer -= timeStep;
			this.getContentPane().remove(geSideMenu);
			this.getContentPane().remove(kaboomSplash);
			this.getContentPane().remove(bombSplash);
			this.getContentPane().remove(healthSplash);
			this.getContentPane().remove(reverseSplash);
			
			if (mysterySplashType == 1){
			
				this.getContentPane().add(bombSplash,  BorderLayout.EAST);
				
			} else if (mysterySplashType == 2){
				this.getContentPane().add(healthSplash,  BorderLayout.EAST);
			} else if (mysterySplashType == 3){
				this.getContentPane().add(reverseSplash,  BorderLayout.EAST);
			} else if (mysterySplashType == 4){
				this.getContentPane().add(kaboomSplash,  BorderLayout.EAST);
			}

			this.revalidate();
			this.repaint();
			
			
			
		} else if (mysterySplashType != 0 && mysterySplashTimer < now){
			mysterySplashTimer = 0;
			mysterySplashType = 0;

			if (mysterySplashType == 1){
				
				this.getContentPane().remove(bombSplash);
				this.bombSplash = null;
				
			} else if (mysterySplashType == 2){
				this.getContentPane().remove(healthSplash);
				
			} else if (mysterySplashType == 3){
				
				this.getContentPane().remove(reverseSplash);
			} else if (mysterySplashType == 4){
				this.getContentPane().remove(kaboomSplash);
			}
			
			this.getContentPane().removeAll();
			this.getContentPane().add(gv, BorderLayout.CENTER);
			this.getContentPane().add(geSideMenu, BorderLayout.EAST);
			this.validate();
			this.pack();
			this.repaint();
			
		} else {
			
			
		}
	}
	
	
	
	public int getGameSize() {
		return this.gameSize;
	}
	public int getTileSize() {
		return this.tileSize;
	}
	public String getTileSrc() {
		return this.tileSrc;
	}
	
	public GameState getGameState() {
		return GameEngine.gameState;
	}
	
	public void setGameState(GameState s) {
		GameEngine.gameState = s;
	}
	
	public int getCurrentLevel() {
		return this.currentLevel;
	}

	public Tile[][] replicateMap(Tile[][] oldMap){
		Tile[][] newMap = new Tile[oldMap.length][oldMap.length];
		for (int i = 0; i < oldMap.length ; i ++){
			for (int j = 0; j < oldMap.length; j ++){
				newMap[i][j] = new Tile(oldMap[i][j].getPosition(), oldMap[i][j].getImgSrc(), oldMap[i][j].getTileSize(), oldMap[i][j].getWall());
			}
		}
		
		return newMap;
	}
}
